#include <stdio.h>
#include <float.h>
#include <limits.h>

main()
{
	printf("int min = %d\n", INT_MIN);
	printf("int max = %d\n", INT_MAX);
	printf("long min = %d\n", LONG_MIN);
	printf("long max = %d\n", LONG_MAX);
	printf("char min = %d\n", CHAR_MIN);
	printf("char max = %d\n", CHAR_MAX);
	printf("float min = %f\n", FLT_MIN);
	printf("float max = %f\n", FLT_MAX);
	printf("double min = %f\n", DBL_MIN);
	printf("double max = %f\n", DBL_MAX);
}